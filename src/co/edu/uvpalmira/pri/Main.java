/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri;

import co.edu.uvpalmira.pri.model.Agente;
import co.edu.uvpalmira.pri.model.Categoria;
import co.edu.uvpalmira.pri.model.Licencia;
import co.edu.uvpalmira.pri.model.OrganismoTransito;
import co.edu.uvpalmira.pri.model.Persona;
import co.edu.uvpalmira.pri.model.Vehiculo;
import co.edu.uvpalmira.pri.ui.TransitoUI;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SISTEMAS
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        OrganismoTransito org = new OrganismoTransito("Palmira");

        /**
         * Creación de objetos para pruebas
         */
        try {
            org.add(new Agente(16000000, "Rico", "Pasto", 101));
            org.add(new Vehiculo("DBC027", "Chevrolet", (short)2002, "Plata"));
            // java.util.Date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date fecha = sdf.parse("2020/12/31");
            System.out.println(fecha);
            org.add(new Licencia(new Persona(111111, "Antonio J", "Vélez Q"), fecha, Categoria.A1));
            org.add(new Licencia(new Persona(111111, "Antonio J", "Vélez Q"), fecha, Categoria.A2));
            org.add(new Licencia(new Persona(111111, "Antonio J", "Vélez Q"), fecha, Categoria.B1));
            org.add(new Licencia(new Persona(111111, "Antonio J", "Vélez Q"), fecha, Categoria.B2));
            org.add(new Licencia(new Persona(111111, "Antonio J", "Vélez Q"), fecha, Categoria.C1));
        } catch (Exception exc) {
        }

        TransitoUI transitoUI = new TransitoUI(org);
        transitoUI.setVisible(true);
    }
}

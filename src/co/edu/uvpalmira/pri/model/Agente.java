/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

/**
 *
 * @author SISTEMAS
 */
public class Agente extends Persona {

    private long numeroPlaca;

    public Agente(long identificacion, String nombre, String apellido, long numeroPlaca) {
        super(identificacion, nombre, apellido);
        this.numeroPlaca = numeroPlaca;
    }

    public long getNumeroPlaca() {
        return numeroPlaca;
    }
    
}

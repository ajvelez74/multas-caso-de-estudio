/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

import java.util.Objects;

/**
 *
 * @author SISTEMAS
 */
public class Vehiculo {

    private String placa;
    private String marca;
    private short modelo;
    private String color;

    public Vehiculo(String placa, String marca, short modelo, String color) throws Exception {
        if(placa == null || placa.trim().isEmpty()) 
            throw new Exception();
        if(marca == null || marca.trim().isEmpty()) 
            throw new Exception();
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        return true;
    }
}

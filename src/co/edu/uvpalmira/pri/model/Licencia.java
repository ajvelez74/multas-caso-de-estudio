/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author SISTEMAS
 */
public class Licencia {

    private Persona propietario;
    private Date expedicion;
    private Categoria categoria;

    public Licencia(Persona propietario, Date expedicion, Categoria categoria) {
        this.propietario = propietario;
        this.expedicion = expedicion;
        this.categoria = categoria;
    }

    public Persona getPropietario() {
        return propietario;
    }

    public Date getExpedicion() {
        return expedicion;
    }

    public void setExpedicion(Date expedicion) {
        this.expedicion = expedicion;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Licencia other = (Licencia) obj;
        if (!Objects.equals(this.propietario, other.propietario)) {
            return false;
        }
        if (this.categoria != other.categoria) {
            return false;
        }
        return true;
    }
}

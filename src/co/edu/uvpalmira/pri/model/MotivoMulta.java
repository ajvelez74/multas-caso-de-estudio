/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

/**
 *
 * @author SISTEMAS
 */
public class MotivoMulta {

    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public short getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getValor() {
        return valor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MotivoMulta other = (MotivoMulta) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "$ " + valor + ".0 -- " + descripcion;
    }
    
    
}

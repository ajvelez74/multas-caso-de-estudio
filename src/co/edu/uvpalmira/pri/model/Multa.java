/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author SISTEMAS
 */
public class Multa {

    private List<MotivoMulta> motivos = new LinkedList<>();
    private Agente agente;
    private Date fecha;
    private Persona conductor;
    private Vehiculo vehiculo;
    private int valor = 0;

    public Multa(Agente agente, Date fecha, Persona conductor, Vehiculo vehiculo) {
        this.agente = agente;
        this.fecha = fecha;
        this.conductor = conductor;
        this.vehiculo = vehiculo;
    }

    public Multa(Agente agente) {
        this.agente = agente;
    }

    public Multa() {
    }

    public List<MotivoMulta> getMotivos() {
        return motivos;
    }

    public Agente getAgente() {
        return agente;
    }

    public Date getFecha() {
        return fecha;
    }

    public Persona getConductor() {
        return conductor;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public int getValor() {
        return valor;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public void add(MotivoMulta motivo) throws Exception {
        if (this.motivos.contains(motivo)) {
            throw new Exception("El motivo " + motivo.getDescripcion() + " ya esta registrado en la multa");
        }
        this.motivos.add(motivo);
        this.valor += motivo.getValor();
    }

    public void remove(MotivoMulta motivoMulta) {
        this.motivos.remove(motivoMulta);
        this.valor -= motivoMulta.getValor();
    }
}

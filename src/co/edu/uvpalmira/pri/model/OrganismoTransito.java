/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvpalmira.pri.model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author SISTEMAS
 */
public class OrganismoTransito {

    private final String ciudad;
    private final List<Licencia> licencias = new LinkedList<>();
    private final List<Agente> agentes = new LinkedList<>();
    private final List<MotivoMulta> motivoMultas = new LinkedList<>();
    private final List<Vehiculo> vehiculos = new LinkedList<>();
    private final List<Multa> multas = new LinkedList<>();

    public OrganismoTransito(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void add(Licencia licencia) {
        if (this.licencias.contains(licencia)) {
        }
        this.licencias.add(licencia);
    }

    public void add(Agente agente) throws Exception {
        if (this.agentes.contains(agente)) {
            throw new Exception("El agente " + agente.getNombre() + " ya se encuentra registrado");
        }
        this.agentes.add(agente);
    }

    public void add(MotivoMulta motivoMulta) throws Exception {
        if (this.motivoMultas.contains(motivoMulta)) {
            throw new Exception("El motivo de multa" + motivoMulta.getDescripcion() + " ya se encuentra registrado");
        }
        this.motivoMultas.add(motivoMulta);
    }

    public void add(Vehiculo vehiculo) throws Exception {
        if (this.vehiculos.contains(vehiculo)) {
            throw new Exception("El vehiculo con placa " + vehiculo.getPlaca() + " ya se encuentra registrado");
        }
        this.vehiculos.add(vehiculo);
    }

    public void add(Multa multa) throws Exception {
        if (multa.getAgente() == null) {
            throw new Exception("El agente que impone la multa no puede ser null");
        }
        if (multa.getConductor() == null) {
            throw new Exception("El conductor al que se le impone la multa no puede ser null");
        }
        if (multa.getFecha() == null) {
            throw new Exception("La fecha en que se impone la multa no puede ser null");
        }
        if (multa.getVehiculo() == null) {
            throw new Exception("El vehiculo no puede ser null");
        }
        if (multa.getMotivos().isEmpty()) {
            throw new Exception("Se debe  especificar el motivo o motivos que generan la multa");
        }
        this.multas.add(multa);
    }

    public List<Licencia> getLicencias() {
        return licencias;
    }

    public List<Agente> getAgentes() {
        return agentes;
    }

    public List<MotivoMulta> getMotivoMultas() {
        return motivoMultas;
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public List<Multa> getMultas() {
        return multas;
    }

    public Agente buscarAgente(long numeroPlaca) throws Exception {
        for (Agente agente : this.agentes) {
            if (agente.getNumeroPlaca() == numeroPlaca) {
                return agente;
            }
        }
        throw new Exception("No se encontró el agente con la placa numero" + numeroPlaca);
    }

    public Licencia buscarConductor(long identificacion) throws Exception {
        for (Licencia licencia : this.licencias) {
            if (licencia.getPropietario().getIdentificacion() == identificacion) {
                return licencia;
            }
        }
        throw new Exception("No se encontró la licencia de la persona identificada con NUIP " + identificacion);
    }

    public List<Licencia> buscarLicencias(long identificacion)  {
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencia : this.licencias) {
            if (licencia.getPropietario().getIdentificacion() == identificacion) {
                encontradas.add(licencia);
            }
        }
        return encontradas;
    }

    public MotivoMulta buscarMotivoMulta(short codigo) throws Exception {
        for (MotivoMulta motivoMulta : this.motivoMultas) {
            if (motivoMulta.getCodigo() == codigo) {
                return motivoMulta;
            }
        }
        throw new Exception("No se encontró un motivo de multa con código " + codigo);
    }

    public Vehiculo buscarVehiculo(String placa) throws Exception {
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo.getPlaca().equalsIgnoreCase(placa)) {
                return vehiculo;
            }
        }
        throw new Exception("No se encontró el vehículo de placa " + placa);
    }
}
